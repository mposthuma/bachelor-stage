function pb_vGenExp_VisFlash(varargin)
%% Initialization
% Clear, empty
 pb_clean;
 disp('>> GENERATING VC EXPERIMENT <<');
 disp('   ...')

 cfn = 0;
%General Values
GV.showexp     = pb_keyval('showexp',varargin,true);
GV.expfile     = pb_keyval('fname',varargin,'MainExp');
GV.datdir      = pb_keyval('datdir',varargin,'U:\Documents\BIOLOGIE\STAGE\MATLAB\EXP-files\MainExp');
GV.cdir        = pb_keyval('cdir',varargin,userpath);
GV.ITI         = pb_keyval('ITI',varargin,[0 0]);
GV.lab         = pb_keyval('lab',varargin,5);
GV.stim        = pb_keyval('stim',varargin,2);

expfile = 'ExpFile';
datdir  = 'U:\Documents\BIOLOGIE\STAGE\MATLAB\EXP-files\MainExp';

cd 'U:\Documents\BIOLOGIE\STAGE\MATLAB\EXP-files\MainExp'
   
%% Desired azimuth and elevation
%  Define hemisphere

%  Select target ranges
maxAbsAz       = 90;
maxAbsEl       = 30;

%  Possible targets
dAz         = [-40 0 40 -30 -30 0 0 30 30 -20 -20 0 0 20 20 90 90 90 90 90 90 90 90 90 90 90 90 90 ]; 
dEl         = [0 0 0 11 -11 11 -11 11 -11 21 -21 21 -21 21 -21 -14 -12 -10 -8 -6 -4 -2 0 2 4 6 8 10 12];

% Restrictions in SLCs if needed
dEl         = dEl(dEl~=0);     % remove elevation = 0
dAz         = dAz(dAz>=-35);
dAz         = dAz(dAz<=35);    %gives 48 stimulus locations



% Make grid
[dAz,dEl]   = meshgrid(dAz,dEl);
dAz         = dAz(:);
dEl         = dEl(:);

sel1		= (abs(dAz)+abs(dEl)) <= maxAbsAz & abs(dEl) <= maxAbsEl;
sel2        = iseven(dAz) | dEl == 0;
sel         = sel1 & sel2;

dAz         = dAz(sel);
dEl         = dEl(sel);
nloc        = numel(dAz);

%% Actual azimuth and elevation
% The actual speaker positions are not perfectly aligned with 5 deg

cfg			= pb_vLookup();                  % sphere positions
channel		= cfg.interpolant(dAz',dEl');
X           = cfg.lookup(channel+1,4);
Y           = cfg.lookup(channel+1,5);

%% Graphics

if GV.showexp
    cfn = pb_newfig(cfn);
    hold on;
    plot(dAz,dEl,'o')
    hold on
    plot(X,Y,'x')
    
    axis([-90 90 -90 90]);
    axis square
    set(gca,'TickDir','out');
    xlabel('Azimuth (deg)');
    ylabel('Elevation (deg)');
    pb_nicegraph;
end
 
%% Stimulus Condition
   
   % FIXATION LED
   fixled.bool    = false;    % no fixation light
   fixled.x       = 0;
   fixled.y       = 0;
   fixled.dur     = 1000;     % 1, 2, 4, 16 ms (?)
   fixled.pause   = 1000;
   
   modality       = 2;                 % 2=VISUAL
   dur            = [0.5 1 2 4 16];                % stim duration in ms 2.^(0:7)*.5
   col            = [1];               % w/ [R,G]
   int			  = randi([30 50],size(dur));              % w/ [i1, i2, i3...]
   
  [X,~,~]                 = ndgrid(X,0,col,int,dur);
  [Y,~,col,int,dur]       = ndgrid(Y,0,col,int,dur);
 
   % Vectorizer
   X              = X(:);
   Y              = Y(:);
   int            = int(:);
   col            = col(:);
   dur            = dur(:);
   
   R    = randperm(length(dur));
   Rdur = dur(R);
   RX   = X(R);
   RY   = Y(R);
   %% Number and size
   Sz			  = size(X);
   N              = Sz(1); % number of trials
   
   maxtrialdur    = max(dur);
   maxtrialdur    = ceil(maxtrialdur/500)/2;

   %% Vestibular blocks
   nblocks           = 1;
   blockdur          = 200;                           % block duration in seconds
   bdureff           = blockdur-(8*pi);               % start & stop
   trialsinblock     = bdureff / (maxtrialdur+1);     % trials per block
   totaltime         = ceil(N*(maxtrialdur+1)) + 8*pi;
   
   blockconditions   = [];
   nblockreps        = N/trialsinblock;

   block.Horizontal  = struct('Amplitude', 0,  'Signal', 1, 'Duration',  blockdur,   'Frequency',0);
   block.Vertical    = struct('Amplitude', 70,  'Signal', 2, 'Duration',  blockdur,   'Frequency',1/(2*pi));
 
   %% Save data somewhere
   writeexp(expfile,datdir,X,Y,int,dur,block,fixled,GV); 
   
end

function writeexp(expfile,datdir,theta,phi,int,dur,block,fixled,GV)
% Save known trial-configurations in exp-file

   expfile     = 'Expfile';
   expfile	   = fcheckext(expfile,'.exp');    % check whether the extension exp is included
   fid         = fopen(expfile,'wt+');         % this is the way to write date to a new file
   
   trialsz     = numel(theta);                  % number of trials
   tdurexp     = 2.1 * trialsz + (sum(dur)/1000);
   effblockdur = 200-(4*pi);
   blocksz     = ceil(tdurexp/effblockdur);     % xnumber of blocks
   leftover    = ceil(mod(tdurexp,effblockdur)+4*pi);
   trlIdx      = 1;                             % trial count
   
   ITI			= [0 0];    % useless, but required in header
   Rep			= 1;        % we have 0 repetitions, so insert 1...
   Rnd			= 0;        % we randomized ourselves already
   Mtr			= 'n';      % the motor should be on
   nblocks      = 1;
   ntrials      = 30;
   pb_vWriteHeader(fid,nblocks,ntrials,GV); % helper-function
   pl    = randperm(trialsz);       % randomize trialorder in bik heb zlocks
   
   newBlock = true;
   totalDur = 0;
   iBlock   = 1;
   for iTrial = 1:trialsz
      %  Write trials
      
      if newBlock == true 
            %  Write blocks
            
            if iBlock == blocksz
               block.Vertical.Duration    = leftover;
               block.Horizontal.Duration  = leftover;
            end 
            
            pb_vWriteBlock(fid,iBlock);
            pb_vWriteSignal(fid,block);
            newBlock = false;
            iBlock   = iBlock+1;
      end
      
      pb_vWriteTrial(fid,trlIdx);
      
      VIS            = [];
      VIS.LED        = 'LED'; 
      VIS.X          = theta(pl(iTrial)); 
      VIS.Y          = phi(pl(iTrial)); 
      VIS.Int        = int(pl(iTrial)); 
      VIS.EventOn    = 0; 
      VIS.Onset      = 500 + randi(100,1,1) - 50;
      VIS.EventOff   = 0; 
      VIS.Offset     = VIS.Onset + dur(pl(iTrial));
      VIS            = pb_vFixLed(VIS,fixled,'x',fixled.x,'y',fixled.y,'dur',fixled.dur,'pause',fixled.pause);

      pb_vWriteStim(fid,2,[],VIS);
      
      totalDur = totalDur + VIS.Offset + 1600; % some extra time for running each trial
      if totalDur >= (200-(4*pi))*1000; totalDur = 0; newBlock = true; end
      trlIdx 	= trlIdx+1;
   end
   fclose(fid);
    end
    