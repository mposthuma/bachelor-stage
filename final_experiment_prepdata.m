%% prepdata
load('converted_data_JJH-0022-21-06-23.mat');
l = dir('preprocessed_data*.mat');
if isempty(l)
    Data = pb_vPrepData('stim',1,'store',1);
else
    load(l(1).name)
end