%% Raw data conversion + Save preprocessed Data
cfn = pb_clean('cd','U:\Documents\BIOLOGIE\STAGE\MATLAB\Data\test experiment');

l = dir('converted_data*.mat');
if isempty(l)
    D = pb_convertdata('block_info_JJH-0007-21-04-15-0001.mat');
    save('converted_data_JJH-0001-21-04-15-0001.mat','D');
else
    load(l(1).name)
end

l = dir('preprocessed_data*.mat');
if isempty(l)
    Data = pb_vPrepData('stim',2,'store',1);
else
    load(l(1).name)
end

%%

t = Data.timestamps.optitrack;

correctie = t(1);
t = t-correctie;

Gaze = Data.position.gaze;


cfn = pb_newfig(cfn);
hold on;
axis square;
plot(t,Gaze(:,1))
plot(t,Gaze(:,2))

pb_nicegraph;
legend('azimuth','elevation')
title('Gaze against time')
xlabel('Time')
ylabel('Gaze')

%%
cfn = pb_newfig(cfn);
hold on;

eye = Data.position.pupillabs;
plot(t,eye(:,1))
plot(t,eye(:,2))
pb_nicegraph;
legend('azimuth','elevation')
title('Eye against time')
xlabel('Time')
ylabel('Eye')

%%

stim(:,1) = Data.stimuli.azimuth;
stim(:,2) = Data.stimuli.elevation;
ts_stim = Data.timestamps.stimuli;
ts_stim = ts_stim(3:4:252)-correctie;

cfn = pb_newfig(cfn);

for iP = 1:2
    subplot(2,1,iP)
    hold on;
    plot(t,Gaze(:,iP))

    for iS = 1:length(stim)
        target = stim(iS,iP)
        plot(ts_stim,stim(:,iP),'x')
        legend('gaze','target')
        sgtitle('Gaze accuracy to target')
        xlabel('Time');
        ylabel('Azimuth/Elevation');
       
    end
end
pb_nicegraph
%%



for i = 1:3:10
    
    i
end
%% Epoch data

fs          = 120;
duration    = 3;
samples     = fs*duration - 1;
%%

for iB = 1:length(Data.timestamps)
   
   % Empty traces
   E.AzChairEpoched   	= [];
   E.ElChairEpoched    = [];
   E.AzGazeEpoched     = [];
   E.ElGazeEpoched     = [];
   E.AzEyeEpoched      = [];
   E.ElEyeEpoched      = [];
   E.AzHeadEpoched     = [];
   E.ElHeadEpoched     = [];
   
   % Interpolate Gaze
   lsl_opti  	= Data.timestamps(iB).optitrack;
   Data.timestamps(iB).epoch_interp       = 0:1/120:lsl_opti(end)-lsl_opti(1);
   Data.position(iB).gaze_interp(:,1)     = interp1(lsl_opti-lsl_opti(iB), Data.position(iB).gaze(:,1), Data.timestamps(iB).epoch_interp,'pchip')';
   Data.position(iB).gaze_interp(:,2)  	= interp1(lsl_opti-lsl_opti(iB), Data.position(iB).gaze(:,2), Data.timestamps(iB).epoch_interp,'pchip')';
   
   % Interpolate Chair
   CUT_OFF = 203;
   Data.position(iB).chair_interp(:,1)    = interp1(Data.timestamps(iB).chair, Data.position(iB).chair, Data.timestamps(iB).epoch_interp,'pchip')';
   Data.position(iB).chair_interp(Data.timestamps(iB).epoch_interp>CUT_OFF,1)  = zeros(1,sum(Data.timestamps(iB).epoch_interp>CUT_OFF));              % Correct for extrapolation == 0;
   Data.position(iB).chair_interp(:,2)    = zeros(size(Data.position(iB).chair_interp(:,1)));
   
   % Interpolate Eye
   Data.position(iB).eye_interp(:,1)      = interp1(lsl_opti-lsl_opti(iB), Data.position(iB).pupillabs(:,1), Data.timestamps(iB).epoch_interp,'pchip')';
   Data.position(iB).eye_interp(:,2)      = interp1(lsl_opti-lsl_opti(iB), Data.position(iB).pupillabs(:,2), Data.timestamps(iB).epoch_interp,'pchip')';
   
   % Interpolate Head
   Data.position(iB).head_interp(:,1)      = interp1(lsl_opti-lsl_opti(iB), Data.position(iB).optitrack(:,1), Data.timestamps(iB).epoch_interp,'pchip')';
   Data.position(iB).head_interp(:,2)      = interp1(lsl_opti-lsl_opti(iB), Data.position(iB).optitrack(:,2), Data.timestamps(iB).epoch_interp,'pchip')';
   
   
   % Select stimuli indices
   nstim       = length(Data.stimuli(iB).azimuth);
   ntriggers   = length(Data.timestamps(iB).stimuli);
   ind         = 3;
   ext         = 4;

   for iS = 1:nstim
      % epoch for stimuli
      start             = Data.timestamps(iB).stimuli(ind) - lsl_opti(1);
      [~,idx]           = min(abs(Data.timestamps(iB).epoch_interp-start));
      
      % Gaze
      E.AzGazeEpoched  	= [E.AzGazeEpoched, Data.position(iB).gaze_interp(idx:idx+samples,1)'];
      E.ElGazeEpoched  	= [E.ElGazeEpoched, Data.position(iB).gaze_interp(idx:idx+samples,2)'];
      
      % Chair
      E.AzChairEpoched  = [E.AzChairEpoched, Data.position(iB).chair_interp(idx:idx+samples,1)'];
      E.ElChairEpoched 	= [E.ElChairEpoched, Data.position(iB).chair_interp(idx:idx+samples,2)'];
      
      % Eye
      E.AzEyeEpoched   	= [E.AzEyeEpoched, Data.position(iB).eye_interp(idx:idx+samples,1)'];
      E.ElEyeEpoched   	= [E.ElEyeEpoched, Data.position(iB).eye_interp(idx:idx+samples,2)'];

      % Head
      E.AzHeadEpoched   = [E.AzHeadEpoched, Data.position(iB).head_interp(idx:idx+samples,1)'];
      E.ElHeadEpoched   = [E.ElHeadEpoched, Data.position(iB).head_interp(idx:idx+samples,2)'];
      
      ind = ind + ext;
   end
   Data.epoch(iB) = E;
end


fn = strrep('preprocessed_data_JJH-0001-21-04-15-0001.mat','preprocessed','epoched');
save(fn,'Data');
