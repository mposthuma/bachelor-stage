%% Initialize
%Clean everything

cfn = pb_clean('cd','C:\Users\marc_\OneDrive\Documents\Studie\STAGE\MATLAB Code\Data\Recordings\final experiment');

%load epoched data
l = dir('epoched_data*.mat');
for iL = 1:length(l)
    load(l(iL).name);
end

%pa_sac2mat; in command window
%load saccades
l = dir(['sacdet' filesep 'sacdet*.mat']);
load(['sacdet' filesep l(1).name]);

%% Extract data
%pb_vExtractPar.m

%get epoched gaze data traces
azimuth = Data.epoch.AzGazeEpoched;
elevation = Data.epoch.ElGazeEpoched;

% Run over saccades
for iS = 1:length(Sac)
   trial_idx = Sac(iS,1);
   
   %Get timings
   start_trial_idx   = (trial_idx-1)*360+1;
   
   stimOn_idx = start_trial_idx;
   sacOn_idx = Sac(iS,3) + start_trial_idx-1;
   sacOff_idx = Sac(iS,4) + start_trial_idx-1;
   
   %Super sac
   SS(iS,1:4) = Sac(iS,1:4);                        %trial/saccade nr/sacc on sample/sac off sample
   SS(iS,5) = 1/120*1000*Sac(iS,3);                 %saccade of reaction time RT
   SS(iS,6) = 1/120*1000*(Sac(iS,4)-Sac(iS,3));     %duration
   SS(iS,7) = azimuth(sacOn_idx);                   %Azimuth onset
   SS(iS,8) = elevation(sacOn_idx);                 %Elevation onset
   SS(iS,9) = azimuth(sacOff_idx);                  %Azimuth offset
   SS(iS,10) = elevation(sacOff_idx);               %Elevation offset
   SS(iS,11) = SS(iS,9)-SS(iS,7);                   %Azimuth Saccade amplitude
   SS(iS,12) = SS(iS,10)-SS(iS,8);                  %Elevation Saccade amplitude
   SS(iS,13) = Data.stimuli.azimuth(SS(iS,1));      %Target azimuth
   SS(iS,14) = Data.stimuli.elevation(SS(iS,1));    %Target elevation
   SS(iS,15) = max(abs(diff(sqrt(azimuth(sacOn_idx:sacOff_idx).^2+elevation(sacOn_idx:sacOff_idx).^2))*120)); %max velocity
   SS(iS,16) = sqrt(SS(iS,11)^2+SS(iS,12)^2);       %R
end

%%

%criteria
sel_first = SS(:,2)==1;                     %only first saccade
sel_timing = SS(:,5)>80 & SS(:,5)<400;      %between 80-400 ms

sel_SS = sel_first & sel_timing;            %combine all
nSS = SS(sel_SS,:);
n_sacc = sum(sel_SS);


c_title= {'Azimuth','Elevation'};

col = pb_selectcolor(2,2);
cfn = pb_newfig(cfn);

for iS = 1:2
    %get data
    target = nSS(:,12+iS);
    response = nSS(:,10+iS);
    
    %graph
    subplot(1,2,iS);
    hold on;
    axis square;
    title(c_title{iS});
    h(iS) = plot(target,response,'o');
    hr(iS) = pb_regplot(target,response,'data',false);
    xlabel('Target ($^{\circ}$)');
    ylabel('Response ($^{\circ}$)');
    xlim([-50 50]);
    ylim([-50 50]);
    pb_dline;
end
pb_nicegraph;

%color
for iH = length(h)
    h(iH).Color = col(iH,:);
    hr(iH).Color = col(iH,:);    
end

%% Main Sequence vs amplitude

cfn = pb_newfig(cfn);
sgtitle('Main Sequence')
%Peak velocity
velocity = SS(:,15);
amplitude = abs(SS(:,16));
subplot(1,2,1);
hold on;
axis square
h(iS) = plot(amplitude,velocity,'o');
xlabel('Amplitude');
ylabel('Peak Velocity');
xlim([0 50])
ylim([0 800])

%Duration
duration = SS(:,6);
amplitude = abs(SS(:,16));
subplot(1,2,2);
hold on;
axis square
h(iS) = plot(amplitude,duration,'o');
xlabel('Amplitude');
ylabel('Duration');
xlim([0 50])
ylim([0 300])
    
pb_nicegraph;

pb_selectcolor

%criteria
%neem tweede saccade mee als 0.5 zo groot van eerste
%binnen 100 ms vallen van de eerste
%gaze shift- van begin naar eindpunt