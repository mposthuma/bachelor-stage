%plot epoched Azimuth data together in 1 plot.

 AzEye    = Data.epoch.AzEyeEpoched(1:360);
 AzGaze   = Data.epoch.AzGazeEpoched(1:360);
 AzChair  = Data.epoch.AzChairEpoched(1:360);
 AzHead   = Data.epoch.AzHeadEpoched(1:360);
 TargetAz = Data.stimuli.azimuth(1:1);
 
t = 1/120:1/120:3;

cfn = pb_newfig(cfn);
hold on
axis square
plot(t, AzEye);
plot(t, AzGaze);
plot(t, AzChair);
plot(t, AzHead);
plot(t(1), TargetAz,'o');
x = [0 0.23 0.23 0];
y = [-10 -10 50 50];
patch(x, y, 'red');
title('Example Epoched Saccade Azimuth');
legend('AzEye', 'AzGaze', 'AzChair', 'AzHead', 'TargetAz', 'ReactionTime');
xlabel('Time(s)');
ylabel('Response($^{\circ}$)');
pb_nicegraph;

%%

%plot epoched Elevation data together in 1 plot.(2nd trial epoch)
 ElEye   = Data.epoch.ElEyeEpoched(361:720);
 ElGaze  = Data.epoch.ElGazeEpoched(361:720);
 ElChair = Data.epoch.ElChairEpoched(361:720);
 ElHead  = Data.epoch.ElHeadEpoched(361:720);
 TargetEl = Data.stimuli.elevation(1:2);

t = 1/120:1/120:3;

cfn = pb_newfig(cfn); 
hold on
axis square
plot(t, ElEye);
plot(t, ElGaze);
plot(t, ElChair);
plot(t, ElHead);
plot(t(1), TargetEl,'o');
x = [0 0.2 0.2 0];
y = [-5 -5 35 35];
patch(x, y, 'red');
title('Example Epoched Saccade Elevation');
legend('ElEye', 'ElGaze', 'ElChair', 'ElHead', 'TargetEl', 'ReactionTime');
xlabel('Time(s)');
ylabel('Response($^{\circ}$)');
pb_nicegraph;