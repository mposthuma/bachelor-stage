function pb_vGenExp_VisFlash(varargin)
%% Initialization
%  Clear, empty, default imputs

pb_clean;
disp('>> GENERATING VC EXPERIMENT <<');
disp('   ...')

cfn = 0;



showexp     = pb_keyval('showexp',varargin,true);
expfile     = pb_keyval('fname',varargin,'MainExp');
datdir      = pb_keyval('datdir',varargin,'U:\Documents\BIOLOGIE\STAGE\MATLAB\EXP-files\MainExp');
cdir        = pb_keyval('cdir',varargin,userpath);

cd 'U:\Documents\BIOLOGIE\STAGE\MATLAB\EXP-files\MainExp'


%% Desired azimuth and elevation
%  Define hemisphere

%  Select target ranges
maxAbsAz       = 45;
maxAbsEl       = 35;

%  Possible targets
dAz         = -45:05:45;
dEl         = -35:10:35;

% Restrictions in SLCs if needed
dEl         = dEl(dEl~=0); % remove elevation = 0
dAz         = dAz(dAz>=-35);
dAz         = dAz(dAz<=35);    %gives 48 stimulus locations



% Make grid
[dAz,dEl]   = meshgrid(dAz,dEl);
dAz         = dAz(:);
dEl         = dEl(:);

sel1		= (abs(dAz)+abs(dEl)) <= maxAbsAz & abs(dEl) <= maxAbsEl;
sel2        = iseven(dAz) | dEl == 0;
sel         = sel1 & sel2;

dAz         = dAz(sel);
dEl         = dEl(sel);
nloc        = numel(dAz);

%% Actual azimuth and elevation
% The actual speaker positions are not perfectly aligned with 5 deg

cfg			= pb_vLookup();                  % sphere positions
channel		= cfg.interpolant(dAz',dEl');
X           = cfg.lookup(channel+1,4);
Y           = cfg.lookup(channel+1,5);

%% Graphics

if showexp
    cfn = pb_newfig(cfn);
    hold on;
    plot(dAz,dEl,'o')
    hold on
    plot(X,Y,'x')
    
    axis([-60 60 -60 60]);
    axis square
    set(gca,'TickDir','out');
    xlabel('Azimuth (deg)');
    ylabel('Elevation (deg)');
    pb_nicegraph;
end

%% Stimulus Condition

% FIXATION LED
fixled.bool    = false;    % do you want a fixation light?
fixled.x       = 0;
fixled.y       = 0;
fixled.dur     = 500;
fixled.pause   = 0;

% STIMULUS parameters
modality       = 2;                     % 2=VISUAL
int			   = [50];                  % w/ [i1, i2, i3...]
dur            = [1 2 4 16];                 % stim duration in ms
col            = [2];                   % w/ [R,G]
NoS            = length(dur);           % Number of Stimulus conditions

[X,~,~]                 = ndgrid(X,0,col,int,dur);
[Y,~,col,int,dur]       = ndgrid(Y,0,col,int,dur);

% Vectorizer
X              = X(:);
Y              = Y(:);
int            = int(:);
col            = col(:);
dur            = dur(:);


%% Number and size
Sz				= size(X);
N				= Sz(1); % number of total trials

% shuffle
randp  = randperm(N);

X       = X(randp);
Y       = Y(randp);
dur     = dur(randp);


%% Vestibular blocks

BlockDur = 200; % 48*3.125*8*pi + some extra time (for pilot experiment set to 200)

block.Horizontal  = struct('Amplitude', 0,       'Signal', 1, 'Duration',  BlockDur ,   'Frequency',0);
block.Vertical    = struct('Amplitude', 70,       'Signal', 2, 'Duration',  BlockDur ,   'Frequency',1/(2*pi));


%% Save data somewhere
% Chunck up data
blocknum = 1;                                  % 1064 trials / 48 trials per block = 23 blocks
startIdx = 1:38:190;                           % 266 trials per category
stopIdx  = 38:38:190; stopIdx(end+1) = 190;    % indexing

for iF = 1:blocknum  %Write file equal to blocknumbers
    
    % Chunck up the data per block
    Xn    = X(startIdx(iF):stopIdx(iF));
    Yn    = Y(startIdx(iF):stopIdx(iF));
    In    = int(startIdx(iF):stopIdx(iF));
    Dn    = dur(startIdx(iF):stopIdx(iF));
    
    fn = [expfile '_b' num2str(iF,'%03.f')];
    writeexp(fn,datdir,Xn,Yn,In,Dn,block,fixled);
end

% see below, these are helper functions to write an exp-file line by line / stimulus by stimulus

%% Show the exp-file in Wordpad
% for PCs
if showexp
    if ispc
        dos(['"C:\Program Files\Windows NT\Accessories\wordpad.exe" ' expfile ' &']);
    elseif ismac
        system(['open -a BBEdit ' cd filesep expfile]);
    end
end
end

function writeexp(expfile,datdir,theta,phi,int,dur,block,fixled)
% Save known trial-configurations in exp-file
%
%  WRITEEXP WRITEEXP(FNAME,DATDIR,THETA,PHI,ID,LEDON)
%
%  WRITEEXP(FNAME,THETA,PHI,ID,LEDON)
%
%  Write exp-file with file-name FNAME.

expfile	    = fcheckext(expfile,'.exp');    % check whether the extension exp is included
fid         = fopen(expfile,'wt+');         % this is the way to write date to a new file

trialsz     = numel(theta);      % number of trials
blocksz     = length(block);     % xnumber of blocks
trlIdx      = 1;                 % trial count

ITI			= [0 0];    % useless, but required in header
Rep			= 1;        % we have 0 repetitions, so insert 1...
Rnd			= 0;        % we randomized ourselves already
Mtr			= 'n';      % the motor should be on

pb_vWriteHeader(fid,datdir,ITI,blocksz,blocksz*trialsz*Rep,Rep,Rnd,Mtr,'Lab',5); % helper-function


for iBlock = 1:blocksz
    
    %  Write blocks
    
    pb_vWriteBlock(fid,iBlock);
    pb_vWriteSignal(fid,block(iBlock));
    
    for iTrial = 1:trialsz
        %  Write trials
        VIS = [];
        pb_vWriteTrial(fid,trlIdx);
        
        for i = 1:2 % 1 = fix, 2 = stim
            % Fixation light
            if i == 1
                VIS(i).LED        = 'LED';
                VIS(i).X          = 0;
                VIS(i).Y          = 0;
                VIS(i).Int        = int(iTrial);
                VIS(i).EventOn    = 0;
                VIS(i).Onset      = 0;
                VIS(i).EventOff   = 0;
                VIS(i).Offset     = 500;
                
            end
            
            if i == 2
                
                % Assemble struct for line in exp file
                VIS(i).LED        = 'LED';
                VIS(i).X          = theta(iTrial);
                VIS(i).Y          = phi(iTrial);
                VIS(i).Int        = int(iTrial);
                VIS(i).EventOn    = 0;
                VIS(i).Onset      = randi([1500 1750]); % Random delay
                VIS(i).EventOff   = 0;
                VIS(i).Offset     = VIS(i).Onset + dur(iTrial); %(pl(iTrial),iBlock);
                
            end
            % Write visual
            %VIS        = pb_vFixLed(VIS,fixled,'x',fixled.x,'y',fixled.y,'dur',fixled.dur,'pause',fixled.pause);
            
            
            
            
        end
        
        %wirte stimulus in file
        trlIdx 	   = trlIdx+1;
        pb_vWriteStim(fid,2,[],VIS);
    end
    
end




fclose(fid);

end



% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                           %
%       Part of Programmeer Beer Toolbox (PBToolbox)        %
%       Written by: Jesse J. Heckman (2020)                 %
%                                                           %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

