%% Vestibular Signal
% sine wave function plot
cfn = pb_clean;
cfn = pb_newfig(cfn);


A = 70; %amplitude
f = 1/(2*pi); %frequency
t = 0.1:0.1:200; %time vector

w = tukeywin(2000,0.25);
w = w';

y = A*sin(2*pi*f*t).*w;

plot(t,y);
title('Sine wave function VC');
xlabel('Time(s)');
ylabel('Amplitude($^{\circ}$)');

