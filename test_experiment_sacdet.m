%% pb_vSacDet.m
cfn = pb_clean ('cd','U:\Documents\BIOLOGIE\STAGE\MATLAB\Data\test experiment');
path = pb_getdir('cdir',cd);
cd(path)

l = dir('epoched*.mat');
if isempty(1); return; end
fn = l(1).name;n
load(fn);

if ~exist('sacdet','dir')
    mkdir('sacdet')
end
path = [path filesep 'sacdet'];
cd(path)

%%

Dlen = length(Data.epoch);
fs = 120;
duration = 3;
samples = duration*fs;

for iB = 1:Dlen
    fname = fcheckext(['sacdet_' fn(14:end-5) '_block_' num2str(iB) '_azel'] ,'.hv');
    fid = fopen([path filesep fname],'w','l');
    AZEL = [Data.epoch(iB).AzGazeEpoched;Data.epoch(iB).ElGazeEpoched];
    
    fwrite(fid,AZEL,'float');
    fclose(fid);
    
    fn_csv = [path filesep fname];
    VC2csv(fn_csv,fs,samples,1:length(Data.epoch(iB).AzGazeEpoched)/samples);
    
    if ~isfile(fn_csv(1:end-3))==1
        pa_sacdet;
        pause;
    end
end
    