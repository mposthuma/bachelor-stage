%% Clean
  

trial_idx   = SS(:,1);
trial_idxU  = unique(trial_idx);
trial_len   = length(trial_idxU);
 
% Trials
for iT = 1:trial_len    % run over all different trials
   
   current_trial  = trial_idxU(iT);                % what is the current trial
   sel_trial      = trial_idx == current_trial;    % select saccades within current trial
   nr_saccades    = sum(sel_trial);                % number of saccades in  current trial
   
   %  clear saccades
   saccades       = [];
   total_sac      = 0;
   
   saccades       = SS(sel_trial,:);               % select relevant saccades in current trial
   
   % Saccades azimuth (11, saccade amplitude)
   % saccade RT   (5)
   
   
   for iS = 1:nr_saccades  % run over all saccades in a trial
      some_condintion = true;
         
      % condition 1: RT >100 ms
      if saccades(iS,5) < 100; some_condintion = false; end % tijd
      
      
      % condition 2: RT < 400 ms
      if saccades(iS,5) > 400; some_condintion = false; end % tijd
         
      %  condition 3: S(-1) AMP > S(0) AMP
      if iS >= 2
         last_sacc_az = abs(saccades(iS-1,9));
         curr_sacc_az = abs(saccades(iS,9));
         last_sacc_el = abs(saccades(iS-1,10));
         curr_sacc_el = abs(saccades(iS,10));
         
         if curr_sacc_az>last_sacc_az; some_condintion = false; end  % first saccade needs to be the largest
         if curr_sacc_el>last_sacc_el; some_condintion = false; end
      end
      
      current_saccade_amplitude_az = saccades(iS,11); % azimuth
      current_saccade_amplitude_el = saccades(iS,12); %elevation
      if some_condintion
         total_sac_az = total_sac + current_saccade_amplitude_az;
         total_sac_el = total_sac + current_saccade_amplitude_el;
      end
      
      SSS(iT,1) = total_sac_az;     % response azimuth
      SSS(iT,2) = saccades(iS,13);  % target azimuth
      SSS(iT,3) = total_sac_el;     % response elevation
      SSS(iT,4) = saccades(iS,14);  % target elevation
   end
end

SSS = SSS(SSS(:,1)~=0,:);

cfn = pb_newfig(cfn);
subplot(1,2,1)
hold on
h = plot(SSS(:,2),SSS(:,1),'o');
hr = pb_regplot(SSS(:,2),SSS(:,1),'data',false);
pb_dline
axis square
title('Azimuth')
xlabel('Target ($^{\circ}$)');
ylabel('Response ($^{\circ}$)');
xlim([-50 50]);
ylim([-50 50]);

subplot(1,2,2)
h = plot(SSS(:,4),SSS(:,3),'o');
hr = pb_regplot(SSS(:,4),SSS(:,3),'data',false);
pb_dline
axis square
title('Elevation')
xlabel('Target ($^{\circ}$)');
ylabel('Response ($^{\circ}$)');
xlim([-50 50]);
ylim([-50 50]);

pb_nicegraph;

%color
for iH = length(h)
    h(iH).Color = col(iH,:);
    hr(iH).Color = col(iH,:);    
end
%%

%plot epoched Azimuth data together in 1 plot.

 AzEye    = Data.epoch.AzEyeEpoched(1:360);
 AzGaze   = Data.epoch.AzGazeEpoched(1:360);
 AzChair  = Data.epoch.AzChairEpoched(1:360);
 AzHead   = Data.epoch.AzHeadEpoched(1:360);
 TargetAz = Data.stimuli.azimuth(1:1);
 
t = 1/120:1/120:3;

cfn = pb_newfig(cfn);
hold on
plot(t, AzEye);
plot(t, AzGaze);
plot(t, AzChair);
plot(t, AzHead);
plot(t(1), TargetAz,'o');
x = [0 0.23 0.23 0];
y = [-10 -10 50 50];
patch(x, y, 'red');
title('Example Epoched Saccade Azimuth');
legend('AzEye', 'AzGaze', 'AzChair', 'AzHead', 'TargetAz', 'ReactionTime');
xlabel('Time(s)');
ylabel('Response($^{\circ}$)');
axis square;
pb_nicegraph;

%%

%plot epoched Elevation data together in 1 plot.(2nd trial epoch)
 ElEye   = Data.epoch.ElEyeEpoched(361:720);
 ElGaze  = Data.epoch.ElGazeEpoched(361:720);
 ElChair = Data.epoch.ElChairEpoched(361:720);
 ElHead  = Data.epoch.ElHeadEpoched(361:720);
 TargetEl = Data.stimuli.elevation(1:2);

t = 1/120:1/120:3;

cfn = pb_newfig(cfn); 
hold on
plot(t, ElEye);
plot(t, ElGaze);
plot(t, ElChair);
plot(t, ElHead);
plot(t(1), TargetEl,'o');
x = [0 0.2 0.2 0];
y = [-5 -5 35 35];
patch(x, y, 'red');
title('Example Epoched Saccade Elevation');
legend('ElEye', 'ElGaze', 'ElChair', 'ElHead', 'TargetEl', 'ReactionTime');
xlabel('Time(s)');
ylabel('Response($^{\circ}$)');
axis square;
pb_nicegraph;









